<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


/**
 * Returns the theme's images folder path.
 *
 * @return bab_Path
 */
function theme_default_getImagePath($directory)
{
    $addon = bab_getAddonInfosInstance('theme_default');

    $ovidentiapath = realpath('.');

    $uploadPath = new bab_Path($ovidentiapath, 'images',  $addon->getRelativePath(), $directory);
    if (!$uploadPath->isDir()) {
        $uploadPath->createDir();
    }

    return $uploadPath;
}



/**
 * Displays a form to edit the theme configuration.
 */
function theme_default_editConfiguration()
{
    bab_Functionality::includefile('Icons');
    $W = bab_Widgets();
    $page = $W->BabPage();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_default/global');

    $form = $W->Form();

    $form->addClass(Func_Icons::ICON_LEFT_16);

    $siteSitemap = bab_Sitemap::getSiteSitemap();

    $nodeSection = $W->Section(
        theme_default_translate('Navigation nodes'),
        $W->VBoxItems(
            theme_default_LabelledWidget(
                theme_default_translate('Top navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('topNavigationNode')
                    ->setValue($registry->getValue('topNavigationNode'))
            ),
            theme_default_LabelledWidget(
                theme_default_translate('Bottom navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('bottomNavigationNode')
                    ->setValue($registry->getValue('bottomNavigationNode'))
            )
        )
    );
    $faviconImagePicker = $W->ImagePicker();
    $faviconImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(32, 32)
        ->setTitle(theme_default_translate('favicon'))
        ->setName('faviconImage');

    $logoImagePicker = $W->ImagePicker();
    $logoImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_default_translate('Logo image'))
        ->setName('logoImage');

    $imagesSection = $W->Section(
        theme_default_translate('Images'),
        $W->FlowItems(
            $faviconImagePicker,
            $logoImagePicker
        )->setHorizontalSpacing(2, 'em')
    );

    $faviconImageFolder = theme_default_getImagePath('favicon');
    $faviconImagePicker->importPath($faviconImageFolder, 'UTF-8');

    $logoImageFolder = theme_default_getImagePath('logo');
    $logoImagePicker->importPath($logoImageFolder, 'UTF-8');

    $colorSection = $W->Section(
        theme_default_translate('Colors'),
        $W->VBoxItems(
        	theme_default_LabelledWidget(
        			theme_default_translate('Body background color'),
        			$W->ColorPicker()
        			->setName('bodyBackgroundColor')
        			->setValue(substr($registry->getValue('bodyBackgroundColor'), 1))
        			),
            theme_default_LabelledWidget(
                theme_default_translate('Header background color'),
                $W->ColorPicker()
                    ->setName('headerBackgroundColor')
                    ->setValue(substr($registry->getValue('headerBackgroundColor'), 1))
            ),
            theme_default_LabelledWidget(
                theme_default_translate('Header text color'),
                $W->ColorPicker()
                    ->setName('headerTextColor')
                    ->setValue(substr($registry->getValue('headerTextColor'), 1))
            ),
        	theme_default_LabelledWidget(
        		theme_default_translate('Main background color'),
        		$W->ColorPicker()
        		->setName('mainBackgroundColor')
        		->setValue(substr($registry->getValue('mainBackgroundColor'), 1))
        	), 		
        	theme_default_LabelledWidget(
        		theme_default_translate('Main text color'),
        		$W->ColorPicker()
        		->setName('mainTextColor')
        		->setValue(substr($registry->getValue('mainTextColor'), 1))
        	),
        	theme_default_LabelledWidget(
        		theme_default_translate('Button background color'),
        		$W->ColorPicker()
        		->setName('buttonBackgroundColor')
        		->setValue(substr($registry->getValue('buttonBackgroundColor'), 1))
        	),
        	theme_default_LabelledWidget(
        		theme_default_translate('Button text color'),
        		$W->ColorPicker()
        		->setName('buttonTextColor')
        		->setValue(substr($registry->getValue('buttonTextColor'), 1))
        	)
        )
    );

    $form->addItem(
        $W->VBoxItems(
            $W->Frame(null,
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->VBoxItems(
                            $colorSection,
                            $nodeSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-67pc'),
                        $W->VBoxItems(
                            $imagesSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-33pc')
                    )->setVerticalAlign('top')
                )->setVerticalSpacing(2, 'em')
            )->setName('configuration'),

            $W->FlowItems(
                $W->SubmitButton()
                    ->setName('idx[save]')
                    ->setLabel(theme_default_translate('Save configuration')),
                $W->SubmitButton()
                    ->setName('idx[cancel]')
                    ->setLabel(theme_default_translate('Cancel'))
            )->setHorizontalSpacing(1, 'em')

        )->setVerticalSpacing(3, 'em')
    );

    $form->addClass('widget-bordered');

    $form->setHiddenValue('tg', bab_rp('tg'));

    $page->setTitle(theme_default_translate('Theme configuration'));
    $page->addItem($form);

    $page->displayHtml();
}




/**
 * Saves the posted configuration.
 *
 * @param array $configuration
 */
function theme_default_saveConfiguration($configuration)
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_default/global');

    if (isset($configuration['topNavigationNode']) && is_string($configuration['topNavigationNode'])) {
        $registry->setKeyValue('topNavigationNode', $configuration['topNavigationNode']);
    }
    if (isset($configuration['bottomNavigationNode']) && is_string($configuration['bottomNavigationNode'])) {
        $registry->setKeyValue('bottomNavigationNode', $configuration['bottomNavigationNode']);
    }
    if (isset($configuration['bodyBackgroundColor']) && is_string($configuration['bodyBackgroundColor'])) {
    	$value = $configuration['bodyBackgroundColor'];
    	$registry->setKeyValue('bodyBackgroundColor', '#' . $value);
    }
    if (isset($configuration['headerBackgroundColor']) && is_string($configuration['headerBackgroundColor'])) {
    	$value = $configuration['headerBackgroundColor'];
    	$registry->setKeyValue('headerBackgroundColor', '#' . $value);
    }
    if (isset($configuration['headerTextColor']) && is_string($configuration['headerTextColor'])) {
    	$value = $configuration['headerTextColor'];
    	$registry->setKeyValue('headerTextColor', '#' . $value);
    }
    if (isset($configuration['mainBackgroundColor']) && is_string($configuration['mainBackgroundColor'])) {
    	$value = $configuration['mainBackgroundColor'];
    	$registry->setKeyValue('mainBackgroundColor', '#' . $value);
    }
    if (isset($configuration['mainTextColor']) && is_string($configuration['mainTextColor'])) {
    	$value = $configuration['mainTextColor'];
    	$registry->setKeyValue('mainTextColor', '#' . $value);
    }
    if (isset($configuration['buttonBackgroundColor']) && is_string($configuration['buttonBackgroundColor'])) {
    	$value = $configuration['buttonBackgroundColor'];
    	$registry->setKeyValue('buttonBackgroundColor', '#' . $value);
    }
    if (isset($configuration['buttonTextColor']) && is_string($configuration['buttonTextColor'])) {
    	$value = $configuration['buttonTextColor'];
    	$registry->setKeyValue('buttonTextColor', '#' . $value);
    }

    $addon = bab_getAddonInfosInstance('theme_default');


    $W = bab_Widgets();

    $faviconImagesPath = theme_default_getImagePath('favicon');
    try {
        $faviconImagesPath->deleteDir();
    } catch (bab_FolderAccessRightsException $e) { }
    $faviconImagesPath->createDir();

    $faviconImagePicker = $W->ImagePicker()
        ->setName('faviconImage');
    if ($faviconFiles = $faviconImagePicker->getTemporaryFiles()) {
        foreach ($faviconFiles as $faviconFile) {
           rename($faviconFile->getFilePath()->toString(), $faviconImagesPath->toString() . '/' . $faviconFile->getFileName());
        }
    }


    $logoImagesPath = theme_default_getImagePath('logo');
    try {
        $logoImagesPath->deleteDir();
    } catch (bab_FolderAccessRightsException $e) { }
    $logoImagesPath->createDir();

    $logoImagePicker = $W->ImagePicker()
        ->setName('logoImage');
    if ($logoFiles = $logoImagePicker->getTemporaryFiles()) {
        foreach ($logoFiles as $logoFile) {
            /*@var $logoFile Widget_FilePickerItem */
            rename(
                $logoFile->getFilePath()->toString(),
                $logoImagesPath->toString() . '/' . $logoFile->getFileName()
            );
        }
    }



    $faviconImage = "''";
    $faviconImagesPath = theme_default_getImagePath('favicon');
    foreach ($faviconImagesPath as $faviconImagePath) {
        $imageFilename = basename($faviconImagePath->toString());
        $imageFilename = str_replace("'", "%27", $imageFilename);
        $faviconImage = "'" . $GLOBALS['babUrl'] . 'images/' .  $addon->getRelativePath() . 'favicon/' .  $imageFilename . "'";
        break;
    }

    $registry->setKeyValue('faviconImage', $faviconImage);

    $logoImagesPath = theme_default_getImagePath('logo');
    $logoImage = "''";
    foreach($logoImagesPath as $logoImagePath) {
        $imageFilename = basename($logoImagePath->toString());
        $imageFilename = str_replace("'", "%27", $imageFilename);
        $logoImage = "'" . $GLOBALS['babUrl'] . 'images/' . $addon->getRelativePath() . 'logo/' . $imageFilename . "'";
        break;
    }

    $registry->setKeyValue('logoImage', $logoImage);


    /* @var $Less Func_Less */
    $Less = bab_functionality::get('less');

    try {
        $Less->removeCompiledFiles();
    } catch(bab_FileAccessRightsException $e) {
        bab_debug($e->getMessage());
    }

}



// Ex�cution

if (!bab_isUserAdministrator()) {
    $babBody->addError(theme_default_translate('Access denied.'));
    return;
}

$idx = bab_rp('idx', 'edit');


if (is_array($idx)) {
    list($idx,) = each($idx);
}


$msg = bab_rp('msg', null);
$errmsg = bab_rp('errmsg', null);
if (isset($errmsg)) {
    $babBody->addError($errmsg);
}
if (isset($msg)) {
    $babBody->addMessage($msg);
}

switch ($idx) {

    case 'save':
        $addon = bab_getAddonInfosInstance('theme_default');
        $configuration = bab_rp('configuration', array());
        theme_default_saveConfiguration($configuration);
        theme_default_redirect($addon->getUrl().'configuration&idx=edit&msg='.urlencode(theme_default_translate('Configuration saved')));
        break;

    case 'edit':
    default:
        theme_default_editConfiguration();
        break;
}
