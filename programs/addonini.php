; <?php/*

[general]
name                            ="theme_default"
addon_type                      ="THEME"
version                         ="1.0.0"
description                     ="Ovidentia responsive Skin with bootstrap"
description.fr                  ="Skin responsive Ovidentia avec integration bootstrap"
icon                            ="icon.png"
image                           ="thumbnail.png"
delete                          =1
addon_access_control            ="0"
ov_version                      ="8.4.92"

author                          ="Bootstrap51"
encoding                        ="ISO-8859-15"
mysql_character_set_database    ="latin1,utf8"

[recommendations]
site_sitemap_node                ="Custom"

[addons]
jquery                          ="1.11.1.3"
libless                         ="0.3.8.3"
portlets                        ="0.20"


[functionalities]
jquery                            ="Available"


;*/ ?>

