<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';


/**
 * Called by ovidentia core when upgrading addon.
 *
 * @param string $version_base
 * @param string $version_ini
 * @return boolean
 */
function theme_default_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    $addon = bab_getAddonInfosInstance('theme_default');
    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('theme_default');
        $addon->addEventListener('bab_eventBeforePageCreated', 'theme_default_addCssAndJs', 'init.php', -10);
        $addon->registerFunctionality('Ovml/Function/ThemeDefaultGetRegistryValue', 'ovml_configuration.php');
        $addon->registerFunctionality('Ovml/Function/ThemeDefaultImageBase64', 'ovml_configuration.php');
    } else {
        // We set a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
        // It must also be executed after other addon may have changed the global babSkin variable.
        bab_addEventListener('bab_eventBeforePageCreated', 'theme_default_addCssAndJs', 'addons/theme_default/init.php', 'theme_default', -10);

        $func = new bab_functionalities;
        $func->register('Ovml/Function/ThemeDefaultGetRegistryValue', dirname(__FILE__).'/ovml_configuration.php');
        $func->register('Ovml/Function/ThemeDefaultImageBase64', dirname(__FILE__).'/ovml_configuration.php');
    }

    return true;
}



/**
 * Called by ovidentia core when deleting addon.
 */
function theme_default_onDeleteAddon()
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    if (function_exists('bab_removeAddonEventListeners')) {
        $addon = bab_getAddonInfosInstance('theme_default');
        bab_removeAddonEventListeners('theme_default');
        $addon->unregisterFunctionality('Ovml/Function/ThemeDefaultGetRegistryValue');
        $addon->unregisterFunctionality('Ovml/Function/ThemeDefaultImageBase64');
    } else {
        bab_removeEventListener('bab_eventBeforePageCreated', 'theme_default_addCssAndJs', 'addons/theme_default/init.php');
        $func = new bab_functionalities;
        $func->unregister('Ovml/Function/ThemeDefaultGetRegistryValue');
        $func->unregister('Ovml/Function/ThemeDefaultImageBase64');
    }

    return true;
}




/**
 * Adds theme specific css and javascript to the page.
 *
 * Must be executed with a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
 * It must also be executed after other addon may have changed the global babSkin variable.
 */
function theme_default_addCssAndJs()
{
    if (bab_isAjaxRequest()) {
        return;
    }
    global $babSkin;
    if ('theme_default' !== $babSkin) {
        return;
    }

    $babBody = bab_getBody();

    $Icons = bab_Functionality::get('Icons');
    $Icons->includeCss();

    $jquery = bab_functionality::get('jquery');
    $jquery->includeCore();
    
    $addon = bab_getAddonInfosInstance('theme_default');
 
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/bootstrap.min.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/jquery.isotope.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/jquery.slicknav.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/jquery.sticky.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/jquery.visible.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/modernizr.custom.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/scripts.js', true);
    $babBody->addJavascriptFile($addon->getThemePath() . 'scripts/slimbox2.js', true);
    /* @var $Less Func_Less */
    if ($Less = @bab_functionality::get('less')) {
        $variables = theme_default_getLessVariables();
        $path = realpath('.') . '/' . $addon->getThemePath() . 'styles/';
        try {
            $stylesheet = $Less->getCssUrl($path.'style.less', $variables, false);
            $babBody->addStyleSheet($stylesheet);
        } catch (Exception $e) {
            bab_debug($e->getMessage());
        }
    }

}
